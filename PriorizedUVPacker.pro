#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T20:02:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PriorizedUVPacker
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    base/geometrymesh.cpp \
    io/sceneimporter.cpp \
    base/node.cpp \
    base/scene.cpp \
    ui/nodelistmodel.cpp \
    ui/aboutdialog.cpp \
    ui/uvshowview.cpp \
    ui/uvlayoutitem.cpp \
    base/uvpolygon.cpp \
    base/uvvertex.cpp \
    base/uvelement.cpp \
    util/uiUtils.cpp \
    util/utils.cpp \
    base/uvmap.cpp \
    util/uiutils.cpp \
    ui/elementlistmodel.cpp

HEADERS  += mainwindow.h \
    base/geometrymesh.h \
    io/sceneimporter.h \
    base/node.h \
    base/scene.h \
    ui/nodelistmodel.h \
    ui/aboutdialog.h \
    ui/uvshowview.h \
    ui/uvlayoutitem.h \
    base/uvpolygon.h \
    base/uvvertex.h \
    base/uvelement.h \
    Include/fbxsdk/core/arch/fbxalloc.h \
    Include/fbxsdk/core/arch/fbxarch.h \
    Include/fbxsdk/core/arch/fbxdebug.h \
    Include/fbxsdk/core/arch/fbxnew.h \
    Include/fbxsdk/core/arch/fbxstdcompliant.h \
    Include/fbxsdk/core/arch/fbxtypes.h \
    Include/fbxsdk/core/base/fbxarray.h \
    Include/fbxsdk/core/base/fbxbitset.h \
    Include/fbxsdk/core/base/fbxcharptrset.h \
    Include/fbxsdk/core/base/fbxcontainerallocators.h \
    Include/fbxsdk/core/base/fbxdynamicarray.h \
    Include/fbxsdk/core/base/fbxfile.h \
    Include/fbxsdk/core/base/fbxfolder.h \
    Include/fbxsdk/core/base/fbxhashmap.h \
    Include/fbxsdk/core/base/fbxintrusivelist.h \
    Include/fbxsdk/core/base/fbxmap.h \
    Include/fbxsdk/core/base/fbxmemorypool.h \
    Include/fbxsdk/core/base/fbxmultimap.h \
    Include/fbxsdk/core/base/fbxpair.h \
    Include/fbxsdk/core/base/fbxredblacktree.h \
    Include/fbxsdk/core/base/fbxset.h \
    Include/fbxsdk/core/base/fbxstatus.h \
    Include/fbxsdk/core/base/fbxstring.h \
    Include/fbxsdk/core/base/fbxstringlist.h \
    Include/fbxsdk/core/base/fbxtime.h \
    Include/fbxsdk/core/base/fbxtimecode.h \
    Include/fbxsdk/core/base/fbxutils.h \
    Include/fbxsdk/core/math/fbxaffinematrix.h \
    Include/fbxsdk/core/math/fbxdualquaternion.h \
    Include/fbxsdk/core/math/fbxmath.h \
    Include/fbxsdk/core/math/fbxmatrix.h \
    Include/fbxsdk/core/math/fbxquaternion.h \
    Include/fbxsdk/core/math/fbxtransforms.h \
    Include/fbxsdk/core/math/fbxvector2.h \
    Include/fbxsdk/core/math/fbxvector4.h \
    Include/fbxsdk/core/sync/fbxatomic.h \
    Include/fbxsdk/core/sync/fbxclock.h \
    Include/fbxsdk/core/sync/fbxsync.h \
    Include/fbxsdk/core/sync/fbxthread.h \
    Include/fbxsdk/core/fbxclassid.h \
    Include/fbxsdk/core/fbxconnectionpoint.h \
    Include/fbxsdk/core/fbxdatatypes.h \
    Include/fbxsdk/core/fbxemitter.h \
    Include/fbxsdk/core/fbxevent.h \
    Include/fbxsdk/core/fbxeventhandler.h \
    Include/fbxsdk/core/fbxlistener.h \
    Include/fbxsdk/core/fbxloadingstrategy.h \
    Include/fbxsdk/core/fbxmanager.h \
    Include/fbxsdk/core/fbxmodule.h \
    Include/fbxsdk/core/fbxobject.h \
    Include/fbxsdk/core/fbxperipheral.h \
    Include/fbxsdk/core/fbxplugin.h \
    Include/fbxsdk/core/fbxplugincontainer.h \
    Include/fbxsdk/core/fbxproperty.h \
    Include/fbxsdk/core/fbxpropertydef.h \
    Include/fbxsdk/core/fbxpropertyhandle.h \
    Include/fbxsdk/core/fbxpropertypage.h \
    Include/fbxsdk/core/fbxpropertytypes.h \
    Include/fbxsdk/core/fbxquery.h \
    Include/fbxsdk/core/fbxqueryevent.h \
    Include/fbxsdk/core/fbxscopedloadingdirectory.h \
    Include/fbxsdk/core/fbxscopedloadingfilename.h \
    Include/fbxsdk/core/fbxstream.h \
    Include/fbxsdk/core/fbxsymbol.h \
    Include/fbxsdk/core/fbxsystemunit.h \
    Include/fbxsdk/core/fbxxref.h \
    Include/fbxsdk/fileio/collada/fbxcolladaanimationelement.h \
    Include/fbxsdk/fileio/collada/fbxcolladaelement.h \
    Include/fbxsdk/fileio/collada/fbxcolladaiostream.h \
    Include/fbxsdk/fileio/collada/fbxcolladanamespace.h \
    Include/fbxsdk/fileio/collada/fbxcolladatokens.h \
    Include/fbxsdk/fileio/collada/fbxcolladautils.h \
    Include/fbxsdk/fileio/collada/fbxreadercollada14.h \
    Include/fbxsdk/fileio/collada/fbxwritercollada14.h \
    Include/fbxsdk/fileio/fbx/fbxio.h \
    Include/fbxsdk/fileio/fbx/fbxreaderfbx5.h \
    Include/fbxsdk/fileio/fbx/fbxreaderfbx6.h \
    Include/fbxsdk/fileio/fbx/fbxreaderfbx7.h \
    Include/fbxsdk/fileio/fbx/fbxwriterfbx5.h \
    Include/fbxsdk/fileio/fbx/fbxwriterfbx6.h \
    Include/fbxsdk/fileio/fbx/fbxwriterfbx7.h \
    Include/fbxsdk/fileio/fbxbase64coder.h \
    Include/fbxsdk/fileio/fbxexporter.h \
    Include/fbxsdk/fileio/fbxexternaldocreflistener.h \
    Include/fbxsdk/fileio/fbxfiletokens.h \
    Include/fbxsdk/fileio/fbxglobalcamerasettings.h \
    Include/fbxsdk/fileio/fbxgloballightsettings.h \
    Include/fbxsdk/fileio/fbxglobalsettings.h \
    Include/fbxsdk/fileio/fbxgobo.h \
    Include/fbxsdk/fileio/fbximporter.h \
    Include/fbxsdk/fileio/fbxiobase.h \
    Include/fbxsdk/fileio/fbxiopluginregistry.h \
    Include/fbxsdk/fileio/fbxiosettings.h \
    Include/fbxsdk/fileio/fbxiosettingspath.h \
    Include/fbxsdk/fileio/fbxprogress.h \
    Include/fbxsdk/fileio/fbxreader.h \
    Include/fbxsdk/fileio/fbxstatistics.h \
    Include/fbxsdk/fileio/fbxstatisticsfbx.h \
    Include/fbxsdk/fileio/fbxwriter.h \
    Include/fbxsdk/scene/animation/fbxanimcurve.h \
    Include/fbxsdk/scene/animation/fbxanimcurvebase.h \
    Include/fbxsdk/scene/animation/fbxanimcurvefilters.h \
    Include/fbxsdk/scene/animation/fbxanimcurvenode.h \
    Include/fbxsdk/scene/animation/fbxanimevalclassic.h \
    Include/fbxsdk/scene/animation/fbxanimevalstate.h \
    Include/fbxsdk/scene/animation/fbxanimevaluator.h \
    Include/fbxsdk/scene/animation/fbxanimlayer.h \
    Include/fbxsdk/scene/animation/fbxanimstack.h \
    Include/fbxsdk/scene/animation/fbxanimutilities.h \
    Include/fbxsdk/scene/constraint/fbxcharacter.h \
    Include/fbxsdk/scene/constraint/fbxcharacternodename.h \
    Include/fbxsdk/scene/constraint/fbxcharacterpose.h \
    Include/fbxsdk/scene/constraint/fbxconstraint.h \
    Include/fbxsdk/scene/constraint/fbxconstraintaim.h \
    Include/fbxsdk/scene/constraint/fbxconstraintcustom.h \
    Include/fbxsdk/scene/constraint/fbxconstraintparent.h \
    Include/fbxsdk/scene/constraint/fbxconstraintposition.h \
    Include/fbxsdk/scene/constraint/fbxconstraintrotation.h \
    Include/fbxsdk/scene/constraint/fbxconstraintscale.h \
    Include/fbxsdk/scene/constraint/fbxconstraintsinglechainik.h \
    Include/fbxsdk/scene/constraint/fbxconstraintutils.h \
    Include/fbxsdk/scene/constraint/fbxcontrolset.h \
    Include/fbxsdk/scene/constraint/fbxhik2fbxcharacter.h \
    Include/fbxsdk/scene/geometry/fbxblendshape.h \
    Include/fbxsdk/scene/geometry/fbxblendshapechannel.h \
    Include/fbxsdk/scene/geometry/fbxcache.h \
    Include/fbxsdk/scene/geometry/fbxcachedeffect.h \
    Include/fbxsdk/scene/geometry/fbxcamera.h \
    Include/fbxsdk/scene/geometry/fbxcamerastereo.h \
    Include/fbxsdk/scene/geometry/fbxcameraswitcher.h \
    Include/fbxsdk/scene/geometry/fbxcluster.h \
    Include/fbxsdk/scene/geometry/fbxdeformer.h \
    Include/fbxsdk/scene/geometry/fbxgenericnode.h \
    Include/fbxsdk/scene/geometry/fbxgeometry.h \
    Include/fbxsdk/scene/geometry/fbxgeometrybase.h \
    Include/fbxsdk/scene/geometry/fbxgeometryweightedmap.h \
    Include/fbxsdk/scene/geometry/fbxlayer.h \
    Include/fbxsdk/scene/geometry/fbxlayercontainer.h \
    Include/fbxsdk/scene/geometry/fbxlight.h \
    Include/fbxsdk/scene/geometry/fbxlimitsutilities.h \
    Include/fbxsdk/scene/geometry/fbxline.h \
    Include/fbxsdk/scene/geometry/fbxlodgroup.h \
    Include/fbxsdk/scene/geometry/fbxmarker.h \
    Include/fbxsdk/scene/geometry/fbxmesh.h \
    Include/fbxsdk/scene/geometry/fbxnode.h \
    Include/fbxsdk/scene/geometry/fbxnodeattribute.h \
    Include/fbxsdk/scene/geometry/fbxnull.h \
    Include/fbxsdk/scene/geometry/fbxnurbs.h \
    Include/fbxsdk/scene/geometry/fbxnurbscurve.h \
    Include/fbxsdk/scene/geometry/fbxnurbssurface.h \
    Include/fbxsdk/scene/geometry/fbxopticalreference.h \
    Include/fbxsdk/scene/geometry/fbxpatch.h \
    Include/fbxsdk/scene/geometry/fbxproceduralgeometry.h \
    Include/fbxsdk/scene/geometry/fbxshape.h \
    Include/fbxsdk/scene/geometry/fbxskeleton.h \
    Include/fbxsdk/scene/geometry/fbxskin.h \
    Include/fbxsdk/scene/geometry/fbxsubdeformer.h \
    Include/fbxsdk/scene/geometry/fbxsubdiv.h \
    Include/fbxsdk/scene/geometry/fbxtrimnurbssurface.h \
    Include/fbxsdk/scene/geometry/fbxvertexcachedeformer.h \
    Include/fbxsdk/scene/geometry/fbxweightedmapping.h \
    Include/fbxsdk/scene/shading/fbxbindingoperator.h \
    Include/fbxsdk/scene/shading/fbxbindingsentryview.h \
    Include/fbxsdk/scene/shading/fbxbindingtable.h \
    Include/fbxsdk/scene/shading/fbxbindingtablebase.h \
    Include/fbxsdk/scene/shading/fbxbindingtableentry.h \
    Include/fbxsdk/scene/shading/fbxconstantentryview.h \
    Include/fbxsdk/scene/shading/fbxentryview.h \
    Include/fbxsdk/scene/shading/fbxfiletexture.h \
    Include/fbxsdk/scene/shading/fbximplementation.h \
    Include/fbxsdk/scene/shading/fbximplementationfilter.h \
    Include/fbxsdk/scene/shading/fbximplementationutils.h \
    Include/fbxsdk/scene/shading/fbxlayeredtexture.h \
    Include/fbxsdk/scene/shading/fbxlayerentryview.h \
    Include/fbxsdk/scene/shading/fbxoperatorentryview.h \
    Include/fbxsdk/scene/shading/fbxproceduraltexture.h \
    Include/fbxsdk/scene/shading/fbxpropertyentryview.h \
    Include/fbxsdk/scene/shading/fbxsemanticentryview.h \
    Include/fbxsdk/scene/shading/fbxshadingconventions.h \
    Include/fbxsdk/scene/shading/fbxsurfacelambert.h \
    Include/fbxsdk/scene/shading/fbxsurfacematerial.h \
    Include/fbxsdk/scene/shading/fbxsurfacephong.h \
    Include/fbxsdk/scene/shading/fbxtexture.h \
    Include/fbxsdk/scene/fbxaxissystem.h \
    Include/fbxsdk/scene/fbxcollection.h \
    Include/fbxsdk/scene/fbxcollectionexclusive.h \
    Include/fbxsdk/scene/fbxcontainer.h \
    Include/fbxsdk/scene/fbxcontainertemplate.h \
    Include/fbxsdk/scene/fbxdisplaylayer.h \
    Include/fbxsdk/scene/fbxdocument.h \
    Include/fbxsdk/scene/fbxdocumentinfo.h \
    Include/fbxsdk/scene/fbxenvironment.h \
    Include/fbxsdk/scene/fbxgroupname.h \
    Include/fbxsdk/scene/fbxlibrary.h \
    Include/fbxsdk/scene/fbxobjectfilter.h \
    Include/fbxsdk/scene/fbxobjectmetadata.h \
    Include/fbxsdk/scene/fbxobjectscontainer.h \
    Include/fbxsdk/scene/fbxpose.h \
    Include/fbxsdk/scene/fbxreference.h \
    Include/fbxsdk/scene/fbxscene.h \
    Include/fbxsdk/scene/fbxselectionnode.h \
    Include/fbxsdk/scene/fbxselectionset.h \
    Include/fbxsdk/scene/fbxtakeinfo.h \
    Include/fbxsdk/scene/fbxthumbnail.h \
    Include/fbxsdk/scene/fbxvideo.h \
    Include/fbxsdk/utils/fbxclonemanager.h \
    Include/fbxsdk/utils/fbxdeformationsevaluator.h \
    Include/fbxsdk/utils/fbxembeddedfilesaccumulator.h \
    Include/fbxsdk/utils/fbxgeometryconverter.h \
    Include/fbxsdk/utils/fbxmanipulators.h \
    Include/fbxsdk/utils/fbxmaterialconverter.h \
    Include/fbxsdk/utils/fbxnamehandler.h \
    Include/fbxsdk/utils/fbxprocessor.h \
    Include/fbxsdk/utils/fbxprocessorshaderdependency.h \
    Include/fbxsdk/utils/fbxprocessorxref.h \
    Include/fbxsdk/utils/fbxprocessorxrefuserlib.h \
    Include/fbxsdk/utils/fbxrenamingstrategy.h \
    Include/fbxsdk/utils/fbxrenamingstrategybase.h \
    Include/fbxsdk/utils/fbxrenamingstrategyfbx5.h \
    Include/fbxsdk/utils/fbxrenamingstrategyfbx6.h \
    Include/fbxsdk/utils/fbxrenamingstrategyfbx7.h \
    Include/fbxsdk/utils/fbxrenamingstrategyutilities.h \
    Include/fbxsdk/utils/fbxrootnodeutility.h \
    Include/fbxsdk/utils/fbxusernotification.h \
    Include/fbxsdk/fbxsdk_def.h \
    Include/fbxsdk/fbxsdk_nsbegin.h \
    Include/fbxsdk/fbxsdk_nsend.h \
    Include/fbxsdk/fbxsdk_version.h \
    Include/fbxsdk.h \
    util/uiUtils.h \
    util/utils.h \
    base/uvmap.h \
    util/uiutils.h \
    ui/elementlistmodel.h

FORMS    += mainwindow.ui \
    ui/aboutdialog.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/ExtLibs/FBX/x64/release/ -llibfbxsdk
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/ExtLibs/FBX/x64/debug/ -llibfbxsdk

INCLUDEPATH += $$PWD/Include
DEPENDPATH += $$PWD/Include

RESOURCES += \
    ui/UIResources.qrc
