#ifndef UIUTILS_H
#define UIUTILS_H

#include <QColor>
#include <QWidget>

namespace uiutils
{
    struct colors
    {
        static QColor GetColorDifference();
        static QColor GetColorHighlight();
        static QColor GetColorDefault();
        static int GetPolygonTransparency();
        static void SetBackgroundByStylesheet(QColor color, int alpha, QWidget* widget);
    };

    struct layout
    {
        static int GetViewSize();
        static int GetViewMargin();
    };
}

#endif // UIUTILS_H
