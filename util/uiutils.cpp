#include "uiutils.h"


QColor uiutils::colors::GetColorDifference()
{
    return QColor(24, 199, 172);
}

QColor uiutils::colors::GetColorHighlight()
{
    return QColor(206, 95, 76);
}

QColor uiutils::colors::GetColorDefault()
{
    return QColor(200, 200, 200);
}

int uiutils::colors::GetPolygonTransparency()
{
    // value between 0 and 255
    return 120;
}


int uiutils::layout::GetViewSize()
{
    return 800;
}

int uiutils::layout::GetViewMargin()
{
    return 10;
}


void uiutils::colors::SetBackgroundByStylesheet(QColor color, int alpha, QWidget* widget)
{
    widget->setStyleSheet(QString("background-color: rgba(%1,%2,%3,%4);").arg(color.red()).arg(color.green()).arg(color.blue()).arg(alpha));
}
