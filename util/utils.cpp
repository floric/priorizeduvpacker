#include "utils.h"

int utils::math::GetRandomID(int low, int high)
{
    return (qrand() % ((high + 1) - low) + low);
}

QColor utils::math::GetRandomColor(bool randomAlpha)
{
    if (randomAlpha) {
        return QColor(GetRandomID(0, 255), GetRandomID(0, 255), GetRandomID(0, 255), GetRandomID(0, 255));
    }
    else {
        return QColor(GetRandomID(0, 255), GetRandomID(0, 255), GetRandomID(0, 255), 255);
    }
}

QColor utils::math::GetRandomColor(int alpha)
{
    return QColor(GetRandomID(0, 255), GetRandomID(0, 255), GetRandomID(0, 255), alpha);
}
