#ifndef UTILS_H
#define UTILS_H

#include <QtGlobal>
#include <QTime>
#include <QColor>

namespace utils
{
    struct math
    {
        static int GetRandomID(int low, int high);
        static QColor GetRandomColor(bool randomAlpha);
        static QColor GetRandomColor(int alpha);
    };
}

#endif // UTILS_H
