#ifndef ELEMENTLISTMODEL_H
#define ELEMENTLISTMODEL_H

#include <QAbstractListModel>

#include "../base/uvelement.h"

class ElementListModel : public QAbstractListModel
{
        Q_OBJECT
    public:
        explicit ElementListModel(QObject* parent = 0);
        void SetElementList(QList<UVElement*> list);
        int rowCount(const QModelIndex& parent) const;
        UVElement* GetElement(int index);
        QVariant data(const QModelIndex& index, int role) const;
        void Clear();

    private:
        QList<UVElement*> elementList;

    signals:

    public slots:
};

#endif // ELEMENTLISTMODEL_H
