#ifndef UVSHOWVIEW_H
#define UVSHOWVIEW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QGraphicsSceneMouseEvent>
#include <QScrollBar>
#include <QDebug>

#include "../base/node.h"
#include "../util/uiUtils.h"

class UVShowView : public QGraphicsView
{
        Q_OBJECT
    public:
        UVShowView(QWidget* parent = 0);
        int getActiveIndex();
        Node* getActiveNode();
        void setActiveNode(Node* node);
        void setActiveIndex(int index);

    protected:
        void mousePressEvent(QMouseEvent* event);
        void mouseReleaseEvent(QMouseEvent* event);
        void wheelEvent(QWheelEvent* event);
        void mouseMoveEvent(QMouseEvent* event);

    private:
        QGraphicsScene* scene;
        QMainWindow* ui;
        int zoomDepth;
        bool isGrabbing;
        qreal zoomFactor = 0.0005;
        qreal grabFactor = -0.02;
        QPoint grabCoords;
        Node* activeNode;
        int activeIndex;
        bool hasActiveNode();

    signals:

    private slots:


};

#endif // UVSHOWVIEW_H
