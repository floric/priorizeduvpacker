#include "uvelementlistmodel.h"

UVElementListModel::UVElementListModel(QObject* parent) :
    QAbstractListModel(parent)
{
    elementList = QVector<UVElement*>();
}

void UVElementListModel::SetElementList(QVector<UVElement*> list)
{
    beginResetModel();

    elementList = list;

    endResetModel();
}

UVElement* UVElementListModel::GetUVElement(int index)
{
    return elementList.at(index);
}

int UVElementListModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return elementList.size();
}

QVariant UVElementListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= elementList.size()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return QVariant(QString("1"));

    } else {
        return QVariant();
    }
}
