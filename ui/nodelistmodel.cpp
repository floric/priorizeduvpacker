#include "nodelistmodel.h"

NodeListModel::NodeListModel(QObject* parent) :
    QAbstractListModel(parent)
{
    nodeList = QList<Node*>();
}

void NodeListModel::SetNodeList(QList<Node*> list)
{
    beginResetModel();

    nodeList = list;

    endResetModel();
}

Node* NodeListModel::GetNode(int index)
{
    return nodeList.at(index);
}

int NodeListModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return nodeList.size();
}

QVariant NodeListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= nodeList.size()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return QVariant(QString("%1").arg((nodeList.at(index.row()))->GetName()));

    } else {
        return QVariant();
    }
}
