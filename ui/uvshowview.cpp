#include "uvshowview.h"

UVShowView::UVShowView(QWidget* parent)
    : QGraphicsView(parent)
{
    scene = new QGraphicsScene(this);
    int sceneSize = uiutils::layout::GetViewMargin() * 2 + uiutils::layout::GetViewSize();
    int sceneOffset = -uiutils::layout::GetViewMargin();
    scene->setSceneRect(sceneOffset, sceneOffset, sceneSize, sceneSize);

    this->setScene(scene);

    zoomDepth = 1;
    grabCoords = QPoint();
    activeIndex = 0;
    isGrabbing = false;
}

void UVShowView::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MiddleButton) {
        isGrabbing = true;
        grabCoords.setX(event->x());
        grabCoords.setY(event->y());
        setCursor(Qt::ClosedHandCursor);
        event->accept();

        return;
    }
    event->ignore();
}

void UVShowView::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MiddleButton) {
        setCursor(Qt::ArrowCursor);
        isGrabbing = false;
        event->accept();

        return;
    }
    event->ignore();
}

bool UVShowView::hasActiveNode()
{
    if (activeNode != NULL) {
        return true;

    } else {
        return false;
    }
}

int UVShowView::getActiveIndex()
{
    return activeIndex;
}

Node* UVShowView::getActiveNode()
{
    return activeNode;
}

void UVShowView::setActiveNode(Node* node)
{
    this->activeNode = node;
}

void UVShowView::setActiveIndex(int index)
{
    if (activeIndex >= 0) {
        this->activeIndex = index;

    } else {
        this->activeIndex = 0;
    }

}

void UVShowView::mouseMoveEvent(QMouseEvent* event)
{
    if (isGrabbing)
    {
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - grabCoords.x()));
        verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - grabCoords.y()));
        grabCoords.setX(event->x());
        grabCoords.setY(event->y());
        event->accept();
        return;
    }
    event->ignore();

}

void UVShowView::wheelEvent(QWheelEvent* event)
{
    setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    this->scale(1.0 + event->delta()*zoomFactor, 1.0 + event->delta()*zoomFactor);
}
