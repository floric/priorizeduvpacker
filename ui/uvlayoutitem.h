#ifndef UVLAYOUTITEM_H
#define UVLAYOUTITEM_H

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>
#include <QVector>

#include "../base/node.h"
#include "../base/uvpolygon.h"
#include "../base/uvelement.h"
#include "../base/uvvertex.h"
#include "../ui/uvshowview.h"
#include "../util/uiUtils.h"

#include "fbxsdk.h"

class UVLayoutItem : public QGraphicsItem
{
    public:
        UVLayoutItem(UVShowView* view);

        QRectF boundingRect() const;

        //overriding paint()
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);
        void setUVView(UVShowView* view);
        UVShowView* getUVView();

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent* event);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

    private:
        UVShowView* activeView;
};

#endif // UVLAYOUTITEM_H
