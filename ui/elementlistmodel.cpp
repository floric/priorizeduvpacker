#include "elementlistmodel.h"

ElementListModel::ElementListModel(QObject* parent) :
    QAbstractListModel(parent)
{
    elementList = QList<UVElement*>();
}

void ElementListModel::SetElementList(QList<UVElement*> list)
{
    beginResetModel();

    elementList = list;

    endResetModel();
}

UVElement* ElementListModel::GetElement(int index)
{
    return elementList.at(index);
}

int ElementListModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return elementList.size();
}

QVariant ElementListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= elementList.size()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return QVariant(QString("Element %1").arg(index.row()));

    } else {
        return QVariant();
    }
}

void ElementListModel::Clear()
{
    elementList = QList<UVElement*>();
}
