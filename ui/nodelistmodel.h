#ifndef NODELISTMODEL_H
#define NODELISTMODEL_H

#include <QAbstractListModel>
#include "../base/node.h"

class NodeListModel : public QAbstractListModel
{
        Q_OBJECT
    public:
        explicit NodeListModel(QObject* parent = 0);
        void SetNodeList(QList<Node*> list);
        int rowCount(const QModelIndex& parent) const;
        Node* GetNode(int index);
        QVariant data(const QModelIndex& index, int role) const;

    private:
        QList<Node*> nodeList;

    signals:

    public slots:

};

#endif // NODELISTMODEL_H
