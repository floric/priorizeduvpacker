#ifndef UVELEMENTLISTMODEL_H
#define UVELEMENTLISTMODEL_H

#include <QAbstractListModel>
#include <QVector>

#include "../base/uvelement.h"

class UVElementListModel : public QAbstractListModel
{
        Q_OBJECT
    public:
        explicit UVElementListModel(QObject* parent = 0);
        void SetElementList(QVector<UVElement*> list);
        int rowCount(const QModelIndex& parent) const;
        UVElement* GetUVElement(int index);
        QVariant data(const QModelIndex& index, int role) const;

    private:
        QVector<UVElement*> elementList;

    signals:

    public slots:

};

#endif // UVELEMENTLISTMODEL_H
