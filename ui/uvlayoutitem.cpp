#include "uvlayoutitem.h"

UVLayoutItem::UVLayoutItem(UVShowView* view)
{
    this->activeView = view;
}

QRectF UVLayoutItem::boundingRect() const
{
    return QRectF(0, 0, uiutils::layout::GetViewSize(), uiutils::layout::GetViewSize());
}

void UVLayoutItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    QPen selectionPen(uiutils::colors::GetColorHighlight());
    QPen defaultPen(uiutils::colors::GetColorDefault());
    selectionPen.setWidth(0);
    defaultPen.setWidth(0);

    int activeIndex = activeView->getActiveIndex();
    Node* activeNode = activeView->getActiveNode();

    if (activeIndex >= activeNode->GetUVMapCount() || activeIndex < 0) {
        activeIndex = 0;
    }

    UVMap* uvMap = activeNode->GetUVMapAt(activeIndex);

    // draw polygons
    for (int elementIndex = 0; elementIndex < uvMap->GetElementCount(); ++elementIndex) {
        UVElement* uvElement = uvMap->GetElement(elementIndex);
        painter->setBrush(QBrush(uvElement->GetColor(), Qt::SolidPattern));

        if (uvElement->IsSelected()) {
            painter->setPen(selectionPen);

        } else {
            painter->setPen(defaultPen);
        }

        QVector<UVPolygon*>* polygons = uvElement->GetPolygons();

        for (int polygonIndex = 0; polygonIndex < polygons->size(); ++polygonIndex) {
            painter->drawPolygon(polygons->at(polygonIndex)->GetQtPolygon());
        }
    }
}

void UVLayoutItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void UVLayoutItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

void UVLayoutItem::setUVView(UVShowView* view)
{
    this->activeView = view;
}

UVShowView* UVLayoutItem::getUVView()
{
    return activeView;
}
