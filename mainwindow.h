#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QItemSelectionModel>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsRectItem>
#include <QRect>
#include <QBrush>
#include <QPen>
#include <QColor>
#include <QColorDialog>

#include <QFileDialog>
#include <QDir>
#include <QDebug>

#include "base/geometrymesh.h"
#include "io/sceneimporter.h"
#include "base/scene.h"
#include "ui/nodelistmodel.h"
#include "ui/elementlistmodel.h"
#include "ui/aboutdialog.h"
#include "ui/uvshowview.h"
#include "ui/uvlayoutitem.h"
#include "util/uiUtils.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget* parent = 0);
        ~MainWindow();

    private slots:
        void importSceneSlot();
        void showAboutInfo();
        void changeColor();
        void exitSlot();
        void nodeSelectionChanged(QModelIndex, QModelIndex);
        void elementSelectionChanged(QModelIndex, QModelIndex);
        void channelSelectionChanged(int index);
        void resetViewZoom();
        void priorityChanged(double newValue);

    private:
        Ui::MainWindow* ui;
        bool importScene();
        void showNode(Node* node);
        void changeLabelsToNode(Node* node);
        void changeViewToNode(Node* node);
        UVElement* GetActiveElement();

        Scene scene;
        NodeListModel* nodeListModel;
        ElementListModel* elementListModel;
        QItemSelectionModel* nodeSelectionModel;
        QItemSelectionModel* elementSelectionModel;
        QGraphicsScene* viewGraphics;
        UVShowView* uvView;
        UVLayoutItem* uvItem;
};

#endif // MAINWINDOW_H
