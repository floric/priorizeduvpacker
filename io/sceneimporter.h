#ifndef SCENEIMPORTER_H
#define SCENEIMPORTER_H

#include <QString>
#include <QDebug>
#include <QVector>
#include <QList>
#include <QMap>
#include <QTime>

#include "../base/scene.h"
#include "../base/uvpolygon.h"
#include "../base/uvvertex.h"
#include "../base/uvelement.h"
#include "../util/uiUtils.h"

#include <fbxsdk.h>

class SceneImporter
{
    public:
        SceneImporter(Scene* scene);
        bool ImportScene(QString fileName);
    private:
        void CleanAfterImport();
        bool CreateUVMaps(Node* node);
        UVMap* CreateMap(Node* node, const FbxLayerElementUV* uv);
        bool AddUVsByPolygonVertex(Node* node, const FbxLayerElementUV* uv, UVMap* map);
        bool FindAttachedPolys(UVElement* elem, UVPolygon* polyToSearchFrom, QList<UVPolygon*>* unknownPolygons);
        void CreateBoundingPolygon(UVElement* elem);
        Scene* scene;
};

#endif // SCENEIMPORTER_H
