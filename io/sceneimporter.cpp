#include "sceneimporter.h"

SceneImporter::SceneImporter(Scene* scene)
{
    this->scene = scene;
}

bool SceneImporter::ImportScene(QString fileName) {
    QTime timer = QTime();
    timer.start();

    qDebug() << "Started import of" << fileName << "!";

    // create a SdkManager
    FbxManager* lSdkManager = FbxManager::Create();

    // create an IOSettings object
    FbxIOSettings* ioSettings = FbxIOSettings::Create(lSdkManager, IOSROOT);

    // create an empty scene
    FbxScene* fbxScene = FbxScene::Create(lSdkManager, "");

    // Create an importer.
    FbxImporter* fbxImporter = FbxImporter::Create(lSdkManager, "");

    if (!fbxImporter->Initialize(fileName.toStdString().c_str(), -1, lSdkManager->GetIOSettings())) {
        printf("File not found!");
        return false;

    } else {
        // Initialize the importer by providing a filename and the IOSettings to use
        fbxImporter->Initialize(fileName.toStdString().c_str(), -1, ioSettings);

        // Import the scene.
        fbxImporter->Import(fbxScene);

        for (int i = 0; i < fbxScene->GetNodeCount(); ++i) {
            FbxNode* importedNode = fbxScene->GetNode(i);

            if (importedNode->GetMesh() != NULL) {
                Node* createdNode = new Node(importedNode);
                scene->AddNode(createdNode);

                int msBeforeUVCreation = timer.elapsed();
                CreateUVMaps(createdNode);
                qDebug() << "UV map creation took" << (qreal(timer.elapsed() - msBeforeUVCreation) / 1000) << "seconds!";
            }
        }

        qDebug() << "Import finished after" << ((qreal(timer.elapsed())) / 1000) << "seconds!";

        // Destroy the importer.
        fbxImporter->Destroy();
    }

    return true;
}

void SceneImporter::CleanAfterImport()
{

}

bool SceneImporter::CreateUVMaps(Node* node)
{
    FbxMesh* pMesh = node->GetFbxMesh();

    qDebug() << "Node: " << node->GetName() << " has " << pMesh->GetLayerCount(FbxLayerElement::eUV) << " layers.";

    //TODO Add multi map support
    //for(int i = 0; i < pMesh->GetLayerCount(FbxLayerElement::eUV); ++i) {
    FbxArray<const FbxLayerElementUV*> uvSets = pMesh->GetLayer(0)->GetUVSets();
    UVMap* newMap;

    for (int k = 0; k < uvSets.Size(); ++k) {
        const FbxLayerElementUV* uv = uvSets.GetAt(k);

        newMap = CreateMap(node, uv);
        node->AddUVMap(newMap);
    }

    //}

    return true;
}

UVMap* SceneImporter::CreateMap(Node* node, const FbxLayerElementUV* uv)
{
    UVMap* newMap = new UVMap(uv->GetName());

    switch (uv->GetMappingMode()) {
        case FbxLayerElement::eByEdge:
        case FbxLayerElement::eByPolygon:
        case FbxLayerElement::eByControlPoint:
            qWarning() << "Unsupported mapping mode for " << node->GetName() << "!";
            break;

        case FbxLayerElement::eByPolygonVertex:
            if (AddUVsByPolygonVertex(node, uv, newMap)) {
                for (int i = 0; i < newMap->GetElementCount(); ++i) {
                    UVElement* elem = newMap->GetElement(i);
                    CreateBoundingPolygon(elem);
                }

                qDebug() << "Successfully loaded UVMap " << newMap->GetName() << " for " << node->GetName();

            } else {
                qCritical() << "Error while loading UVMap " << newMap->GetName() << " for " << node->GetName();
            }

            break;

        default:
            qWarning() << "Unknown mapping mode for " << node->GetName() << "!";
            break;
    }

    return newMap;
}

bool SceneImporter::AddUVsByPolygonVertex(Node* node, const FbxLayerElementUV* uv, UVMap* map)
{
    QMap<int, int> geoVertexOccurence = QMap<int, int>();
    QMap<int, int> uvVertexOccurence = QMap<int, int>();

    FbxMesh* pMesh = node->GetFbxMesh();

    FbxLayerElementArrayTemplate<FbxVector2> uvValues = uv->GetDirectArray();
    FbxLayerElementArrayTemplate<int> indexValues = uv->GetIndexArray();

    switch (uv->GetReferenceMode()) {
        case FbxLayerElement::eDirect:
        case FbxLayerElement::eIndex:
            qWarning() << "Unsupported reference mode for " << node->GetName() << " (" << map->GetName() << ")";
            break;

        case FbxLayerElement::eIndexToDirect:

            if (pMesh->GetPolygonVertexCount() == indexValues.GetCount()) {

                UVPolygon* newPoly;
                UVVertex* newVertex;
                FbxGeometryElementUV* uvElem = pMesh->GetElementUV(uv->GetName());
                qreal totalArea = 0;

                QList<UVPolygon*>* listOfPolys = new QList<UVPolygon*>();

                // create uv geometry
                for (int i = 0; i < pMesh->GetPolygonCount(); ++i) {
                    newPoly = new UVPolygon();

                    const int vertexIndexOffset = pMesh->GetPolygonVertexIndex(i);

                    for (int k = 0; k < pMesh->GetPolygonSize(i); ++k) {
                        const int uvVertexIndex = pMesh->GetTextureUVIndex(i, k);
                        const int geoVertexIndex = pMesh->GetPolygonVertices()[vertexIndexOffset + k];

                        newVertex = new UVVertex(geoVertexIndex, uvVertexIndex, uvValues.GetAt(uvVertexIndex)[0], uvValues.GetAt(uvVertexIndex)[1]);

                        newPoly->AddVertex(newVertex);
                    }

                    listOfPolys->append(newPoly);
                }

                qDebug() << "Created UV geometry with" << listOfPolys->size() << "polygons!";

                UVElement* newElem;

                while (!listOfPolys->isEmpty()) {
                    newElem = new UVElement();

                    UVPolygon* currentPoly = listOfPolys->first();

                    if (FindAttachedPolys(newElem, currentPoly, listOfPolys)) {

                    }

                    map->AddElement(newElem);
                    qDebug() << "Created new island with " << newElem->GetPolygonCount() << "polygons, " << listOfPolys->size() << "polygons left!";

                    totalArea += newElem->GetSurfaceArea();
                }

                qDebug() << "Created UV islands!";
                qDebug() << "They use" << totalArea << "areaunits!";

                delete listOfPolys;
            }

            break;

        default:
            qWarning() << "Unknown reference mode for " << node->GetName() << " (" << map->GetName() << ")";
            break;
    }

    return true;
}

bool SceneImporter::FindAttachedPolys(UVElement* elem, UVPolygon* polyToSearchFrom, QList<UVPolygon*>* unknownPolygons)
{
    bool wasSuccessfull = false;
    QSet<UVPolygon*> foundPolygons = QSet<UVPolygon*>();

    // add this poly to the island
    unknownPolygons->removeOne(polyToSearchFrom);
    elem->AddPolygon(polyToSearchFrom);

    // search for connected islands
    for (int i = 0; i < polyToSearchFrom->GetVertexCount(); ++i) {
        UVVertex* vertex = polyToSearchFrom->GetVertex(i);

        foreach (UVPolygon* polyToCompare, *unknownPolygons) {
            for (int k = 0; k < polyToCompare->GetVertexCount(); ++k) {
                UVVertex* compareVertex = polyToCompare->GetVertex(k);

                if (*vertex->GetUvID() == *compareVertex->GetUvID()) {
                    foundPolygons.insert(polyToCompare);
                    unknownPolygons->removeOne(polyToCompare);

                    wasSuccessfull = true;
                    break;
                }
            }
        }
    }

    foreach (UVPolygon* poly, foundPolygons) {
        //while (!foundPolygons.isEmpty()) {
        FindAttachedPolys(elem, poly, unknownPolygons);
        //foundPolygons.remove(f)
    }

    return wasSuccessfull;
}

void SceneImporter::CreateBoundingPolygon(UVElement* elem)
{
    if (elem->GetPolygonCount() > 0) {

    }
}


