#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->statusBar->showMessage(tr("Ready"));

    nodeListModel = new NodeListModel(this);
    elementListModel = new ElementListModel(this);

    connect(ui->actionImport, SIGNAL(triggered()), this, SLOT(importSceneSlot()));
    connect(ui->actionClose, SIGNAL(triggered()), this, SLOT(exitSlot()));
    connect(ui->actionInfo, SIGNAL(triggered()), this, SLOT(showAboutInfo()));
    connect(ui->uvChannelComboxBox, SIGNAL(currentIndexChanged(int)), SLOT(channelSelectionChanged(int)));
    connect(ui->resetViewButton, SIGNAL(clicked()), SLOT(resetViewZoom()));
    connect(ui->elementColorButton, SIGNAL(clicked()), SLOT(changeColor()));
    connect(ui->elementPriority, SIGNAL(valueChanged(double)), SLOT(priorityChanged(double)));

    scene = Scene();

    uvView = new UVShowView(0);
    viewGraphics = new QGraphicsScene();

    ui->verticalLayout->addWidget(uvView);
    ui->verticalLayout->setContentsMargins(10, 10, 10, 10);
    uvView->setScene(viewGraphics);
    uvView->setContentsMargins(10, 10, 10, 10);

    int viewSize = 2 * uiutils::layout::GetViewMargin() + uiutils::layout::GetViewSize();
    int viewOffset = -uiutils::layout::GetViewMargin();
    viewGraphics->setSceneRect(viewOffset, viewOffset, viewSize, viewSize);
}

bool MainWindow::importScene() {
    SceneImporter importer = SceneImporter(&scene);
    QString filePath = QFileDialog::getOpenFileName(
                           this, tr("Import FBX"), QDir::homePath(), tr("FBX (*.FBX)"));

    bool importWasSuccessfull = importer.ImportScene(filePath);

    if (importWasSuccessfull) {
        nodeListModel->SetNodeList(*scene.GetAllNodes());
        ui->nodeListView->setModel(nodeListModel);

        nodeSelectionModel = ui->nodeListView->selectionModel();
        connect(nodeSelectionModel, SIGNAL(currentChanged(QModelIndex, QModelIndex)),
                this, SLOT(nodeSelectionChanged(QModelIndex, QModelIndex)));

        ui->statusBar->showMessage(tr("Successfully imported scene."));

    } else {
        ui->nodeListView->reset();
        ui->statusBar->showMessage(tr("Error during import."));
    }

    return importWasSuccessfull;
}

void MainWindow::showNode(Node* node) {
    uvView->setActiveNode(node);

    changeLabelsToNode(node);
    changeViewToNode(node);

    uvView->update();
}

void MainWindow::changeLabelsToNode(Node* node) {
    FbxMesh* nodeMesh = node->GetFbxMesh();

    QString meshInfo = QString(tr("%1 polygons\n%2 edges\n%3 vertices"))
                       .arg(nodeMesh->GetPolygonCount())
                       .arg(nodeMesh->GetMeshEdgeCount())
                       .arg(nodeMesh->GetControlPointsCount());

    ui->nodeName->setText(node->GetName());
    ui->detailsName->setText(node->GetName());
    ui->detailsMesh->setText(meshInfo);

    ui->uvChannelComboxBox->clear();

    if (node->HasUVLayers()) {
        UVMap* activeMap = node->GetUVMapAt(uvView->getActiveIndex());
        QVector<UVElement*>* elementList = activeMap->GetContent();

        ui->channelLabel->setText(tr("UV channel"));
        ui->uvChannelComboxBox->setVisible(true);

        // add UV channels to selection box
        for (int i = 0; i < node->GetUVMapCount(); ++i) {
            ui->uvChannelComboxBox->addItem(QString::number(i));
        }

        QString uvChannels;

        FbxStringList uvSetNameList;
        nodeMesh->GetUVSetNames(uvSetNameList);

        for (int uvIndex = 0; uvIndex < node->GetUVMapCount(); ++uvIndex) {
            const char* uvSetName = uvSetNameList.GetStringAt(uvIndex);
            uvChannels.append(tr("%1 - %2\n").arg(uvIndex).arg(uvSetName));
        }

        ui->detailsChannels->setText(uvChannels);
        ui->detailsUV->setText(tr("<b>%1 UV channels</b>").arg(node->GetUVMapCount()));

        elementListModel->SetElementList(elementList->toList());
        ui->elementListView->setModel(elementListModel);

        elementSelectionModel = ui->elementListView->selectionModel();
        connect(elementSelectionModel, SIGNAL(currentChanged(QModelIndex, QModelIndex)),
                this, SLOT(elementSelectionChanged(QModelIndex, QModelIndex)));

        ui->mapTotalArea->setText(QString("%1").arg(activeMap->GetSurfaceArea()));

    } else {
        ui->channelLabel->setText(tr("No UVs created!"));
        ui->detailsChannels->setText("");
        ui->detailsUV->setText(tr("No UV channels created!"));
        ui->uvChannelComboxBox->setVisible(false);

        elementListModel->Clear();
    }
}

void MainWindow::changeViewToNode(Node* node) {
    viewGraphics->clear();

    QColor colorBlue = QColor(24, 199, 172);
    QColor colorRed = QColor(206, 95, 76);
    QBrush solidBrush(Qt::SolidPattern);

    QPen detailPen(Qt::white);
    detailPen.setWidth(0);

    QPen emptyPen(Qt::white);
    emptyPen.setWidth(0);

    if (node->HasUVLayers()) {
        QPen borderPen(colorBlue);
        borderPen.setWidth(0);

        viewGraphics->addRect(0, 0, uiutils::layout::GetViewSize(), uiutils::layout::GetViewSize(), borderPen, solidBrush);

        uvItem = new UVLayoutItem(uvView);
        viewGraphics->addItem(uvItem);

    } else {
        viewGraphics->addText(tr("No UV channels known"));
    }
}

UVElement* MainWindow::GetActiveElement()
{
    return elementListModel->GetElement(elementSelectionModel->currentIndex().row());
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::importSceneSlot() {
    importScene();
}

void MainWindow::nodeSelectionChanged(QModelIndex, QModelIndex) {
    showNode(nodeListModel->GetNode(nodeSelectionModel->currentIndex().row()));
}

void MainWindow::elementSelectionChanged(QModelIndex, QModelIndex)
{
    UVElement* activeElement = GetActiveElement();
    uvView->getActiveNode()->GetUVMapAt(uvView->getActiveIndex())->ClearElementSelection();
    activeElement->SetSelected(true);

    QColor activeColor = activeElement->GetColor();
    ui->elementColorButton->setStyleSheet(QString("background-color: rgba(%1,%2,%3,%4);").arg(activeColor.red()).arg(activeColor.green()).arg(activeColor.blue()).arg(uiutils::colors::GetPolygonTransparency()));
    uiutils::colors::SetBackgroundByStylesheet(activeColor, uiutils::colors::GetPolygonTransparency(), ui->elementColorButton);
    ui->elementPriority->setValue(activeElement->GetPriority());
    ui->elementColorButton->setDisabled(false);

    ui->elementAreaLabel->setText(QString("%1").arg(activeElement->GetSurfaceArea()));

    uvView->update();
}

void MainWindow::channelSelectionChanged(int index) {
    if (uvItem != NULL && uvView->getActiveIndex() != index) {
        if (index >= 0) {
            uvView->setActiveIndex(index);
            uvView->update();
        }

    }
}

void MainWindow::resetViewZoom()
{
    uvView->resetTransform();
}

void MainWindow::priorityChanged(double newValue)
{
    GetActiveElement()->SetPriority(newValue);
}

void MainWindow::exitSlot() {
    exit(0);
}

void MainWindow::showAboutInfo() {
    AboutDialog* dialog = new AboutDialog(this);
    dialog->show();
}

void MainWindow::changeColor()
{
    QColor newColor = QColorDialog::getColor(GetActiveElement()->GetColor(), this);

    if (newColor.isValid()) {
        newColor.setAlpha(uiutils::colors::GetPolygonTransparency());
        GetActiveElement()->SetColor(newColor);
        uiutils::colors::SetBackgroundByStylesheet(GetActiveElement()->GetColor(), uiutils::colors::GetPolygonTransparency(), ui->elementColorButton);
    }

}
