#ifndef SCENE_H
#define SCENE_H

#include <QList>

#include "node.h"

class Scene
{
    public:
        Scene();
        void AddNode(Node* node);
        const Node* GetNode(qint16);
        QList<Node*>* GetAllNodes();
        int GetNodesSize();
    private:
        QList<Node*> sceneNodes;
};

#endif // SCENE_H
