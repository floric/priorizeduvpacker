#include "uvmap.h"

UVMap::UVMap(QString name)
{
    this->name = name;
}

UVElement* UVMap::GetElement(int index)
{
    return content.at(index);
}

void UVMap::AddElement(UVElement* elem)
{
    content.append(elem);
}

QVector<UVElement*>* UVMap::GetContent()
{
    return &content;
}

QString UVMap::GetName()
{
    return name;
}

void UVMap::ClearElementSelection()
{
    for (int i = 0; i < content.size(); ++i) {
        (content.at(i))->SetSelected(false);
    }
}

qreal UVMap::GetSurfaceArea()
{
    qreal area = 0;

    for (int i = 0; i < content.size(); ++i) {
        area += content.at(i)->GetSurfaceArea();
    }

    return area;
}

int UVMap::GetElementCount()
{
    return content.size();
}
