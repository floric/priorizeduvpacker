#include "uvelement.h"

UVElement::UVElement()
{
    polygons = QVector<UVPolygon*>();
    priority = 1.0;
    isSelected = false;
    color = utils::math::GetRandomColor(uiutils::colors::GetPolygonTransparency());
    boundingPoly = QPolygon();
}

QVector<UVPolygon*>* UVElement::GetPolygons()
{
    return &polygons;
}

void UVElement::AddPolygon(UVPolygon* poly)
{
    polygons.append(poly);
    poly->SetUVElement(this);
}

void UVElement::ClearPolygons()
{
    polygons.clear();
}

int UVElement::GetPolygonCount()
{
    return polygons.size();
}

bool UVElement::HasPolygons()
{
    return !polygons.isEmpty();
}

qreal UVElement::GetPriority()
{
    return priority;
}

void UVElement::SetPriority(qreal prio)
{
    this->priority = prio;
}

QColor UVElement::GetColor()
{
    return color;
}

void UVElement::SetColor(QColor newColor)
{
    this->color = newColor;
}

bool UVElement::IsSelected()
{
    return isSelected;
}

void UVElement::SetSelected(bool state)
{
    this->isSelected = state;
}

void UVElement::SetBoundingPolygon(QPolygon* poly)
{
    this->boundingPoly = *poly;
}

QPolygon* UVElement::GetBoundingPolygon()
{
    return &boundingPoly;
}

QVector2D* UVElement::GetGeometricCenter()
{
    return &geometricCenter;
}

void UVElement::CacheCenter()
{
    QVector2D newCenter = QVector2D();

    int vertexCount = 0;

    for (int i = 0; i < polygons.size(); ++i) {
        UVPolygon* poly = polygons.at(i);

        for (int k = 0; poly->GetVertexCount(); ++k) {
            UVVertex* vertex = poly->GetVertex(k);

            newCenter.operator += (QVector2D(vertex->GetX(), vertex->GetY()));

            vertexCount++;
        }
    }

    this->geometricCenter.setX(newCenter.x() / vertexCount);
    this->geometricCenter.setY(newCenter.y() / vertexCount);
}

QTransform* UVElement::GetTransformation()
{
    return &elemTransformation;
}

void UVElement::SetTransformation(QTransform transform)
{
    this->elemTransformation = transform;
}

void UVElement::ScaleElem(QVector2D scale)
{
    this->elemTransformation.scale(scale.x(), scale.y());
}

void UVElement::RotateElem(qreal angle)
{
    this->elemTransformation.rotate(angle);
}

void UVElement::TranslateElem(QVector2D offset)
{
    this->elemTransformation.translate(offset.x(), offset.y());
}

qreal UVElement::GetSurfaceArea()
{
    qreal area = 0;

    for (int i = 0; i < polygons.size(); ++i) {
        area += polygons.at(i)->GetSurfaceArea();
    }

    return area;
}
