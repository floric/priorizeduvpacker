#include "scene.h"

Scene::Scene()
{
    sceneNodes = QList<Node*>();
}

void Scene::AddNode(Node* node)
{
    this->sceneNodes.append(node);
}

QList<Node*>* Scene::GetAllNodes()
{
    return &this->sceneNodes;
}

int Scene::GetNodesSize()
{
    return sceneNodes.size();
}

const Node* Scene::GetNode(qint16 id)
{
    return this->sceneNodes.at(id);
}
