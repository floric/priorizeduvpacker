#include "node.h"

Node::Node(FbxNode* fbxNode)
{
    this->fbxNode = fbxNode;
    this->uvMaps = QVector<UVMap*>();
}

QString Node::GetName()
{
    return this->fbxNode->GetName();
}

bool Node::AddUVMap(UVMap* map)
{
    //CreateUVIslands(map);
    uvMaps.push_back(map);

    return true;
}

UVMap* Node::GetUVMapAt(int index)
{
    return uvMaps.at(index);
}

bool Node::SetUVMapAt(int index, UVMap* map)
{
    uvMaps.replace(index, map);
    return true;
}

FbxNode* Node::GetFbxNode()
{
    return fbxNode;
}

FbxMesh* Node::GetFbxMesh()
{
    return fbxNode->GetMesh();
}

bool Node::HasUVLayers()
{
    return !uvMaps.isEmpty();
}

int Node::GetUVMapCount()
{
    return uvMaps.size();
}

