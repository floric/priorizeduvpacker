#ifndef UVMAP_H
#define UVMAP_H

#include <QString>
#include <QVector>

#include "uvelement.h"

class UVMap
{
    public:
        UVMap(QString name);
        QVector<UVElement*>* GetContent();
        UVElement* GetElement(int index);
        void AddElement(UVElement* elem);
        int GetElementCount();
        QString GetName();
        void ClearElementSelection();
        qreal GetSurfaceArea();

    private:
        QVector<UVElement*> content;
        QString name;
};

#endif // UVMAP_H
