#ifndef NODE_H
#define NODE_H

#include <QDebug>
#include <QString>
#include <QVector>
#include <QSet>

#include "uvelement.h"
#include "uvpolygon.h"
#include "uvvertex.h"
#include "uvmap.h"

#include <fbxsdk.h>

class UVMap;

class Node
{
    public:
        Node(FbxNode* fbxNode);
        FbxNode* GetFbxNode();
        FbxMesh* GetFbxMesh();
        QString GetName();
        bool AddUVMap(UVMap* map);
        UVMap* GetUVMapAt(int index);
        bool SetUVMapAt(int index, UVMap* map);
        bool HasUVLayers();
        int GetUVMapCount();

    private:
        FbxNode* fbxNode;
        qreal compareThreshold = 0.001;
        QVector<UVMap*> uvMaps;
};

#endif // NODE_H
