#include "uvpolygon.h"

UVPolygon::UVPolygon()
{
    this->vertices = QVector<UVVertex*>();

    element = new UVElement();
    element->AddPolygon(this);
}

QVector<UVVertex*>* UVPolygon::GetVertices()
{
    return &vertices;
}

void UVPolygon::AddVertex(UVVertex* vertex)
{
    vertices.append(vertex);
}

int UVPolygon::GetVertexCount()
{
    return vertices.size();
}

UVVertex* UVPolygon::GetVertex(int index)
{
    return vertices.at(index);
}

QPolygonF UVPolygon::GetQtPolygon()
{
    QPolygonF poly = QPolygonF();
    UVVertex* vertex;

    for (int vertexIndex = 0; vertexIndex < vertices.size(); ++vertexIndex) {
        vertex = vertices.at(vertexIndex);
        poly.append(QPointF(vertex->GetX() * uiutils::layout::GetViewSize(), (1 - vertex->GetY()) * uiutils::layout::GetViewSize()));
    }

    return poly;
}

void UVPolygon::SetUVElement(UVElement* element)
{
    this->element = element;
    //delete element;
}

UVElement* UVPolygon::GetUVElement()
{
    return element;
}

qreal UVPolygon::GetSurfaceArea()
{
    qreal area = 0;

    for (int vertexIndex = 0; vertexIndex < vertices.size(); ++vertexIndex) {
        UVVertex* vertex = vertices.at(vertexIndex);
        UVVertex* nextVertex;

        if ((vertexIndex + 1) < vertices.size()) {
            nextVertex = vertices.at(vertexIndex + 1);

        } else {
            nextVertex = vertices.at(0);
        }


        area += (vertex->GetX() + nextVertex->GetX()) * (nextVertex->GetY() - vertex->GetY());
    }

    return qAbs(area / 2);
}
