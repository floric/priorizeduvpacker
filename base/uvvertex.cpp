#include "uvvertex.h"

UVVertex::UVVertex(int meshID, int uvID)
{
    SetX(0);
    SetY(0);
    SetMeshID(meshID);
    SetUvID(uvID);
}

UVVertex::UVVertex(int meshID, int uvID, qreal x, qreal y)
{
    SetX(x);
    SetY(y);
    SetMeshID(meshID);
    SetUvID(uvID);
}

UVVertex::UVVertex(int meshID, int uvID, float x, float y)
{
    SetX(x);
    SetY(y);
    SetMeshID(meshID);
    SetUvID(uvID);
}

qreal UVVertex::GetX()
{
    return x;
}

qreal UVVertex::GetY()
{
    return y;
}

int* UVVertex::GetMeshID()
{
    return &meshID;
}

void UVVertex::SetMeshID(int id)
{
    this->meshID = id;
}

int* UVVertex::GetUvID()
{
    return &uvID;
}

void UVVertex::SetUvID(int id)
{
    this->uvID = id;
}

void UVVertex::SetX(qreal x)
{
    this->x = x;
}

void UVVertex::SetY(qreal y)
{
    this->y = y;
}

