#ifndef UVELEMENT_H
#define UVELEMENT_H

#include <QVector>
#include <QPolygon>
#include <QVector2D>
#include <QTransform>

#include "node.h"
#include "../util/utils.h"
#include "../util/uiUtils.h"

class UVPolygon;

class UVElement
{
    public:
        UVElement();
        QVector<UVPolygon*>* GetPolygons();
        void AddPolygon(UVPolygon* poly);
        void ClearPolygons();
        int GetPolygonCount();
        bool HasPolygons();
        qreal GetPriority();
        void SetPriority(qreal prio);
        QColor GetColor();
        void SetColor(QColor newColor);
        bool IsSelected();
        void SetSelected(bool state);
        void SetBoundingPolygon(QPolygon* poly);
        QPolygon* GetBoundingPolygon();
        QVector2D* GetGeometricCenter();
        void CacheCenter();
        QTransform* GetTransformation();
        void SetTransformation(QTransform transform);
        void ScaleElem(QVector2D scale);
        void RotateElem(qreal angle);
        void TranslateElem(QVector2D offset);
        qreal GetSurfaceArea();

    private:
        QVector<UVPolygon*> polygons;
        QVector2D geometricCenter;
        qreal priority;
        QColor color;
        bool isSelected;
        QPolygon boundingPoly;
        QTransform elemTransformation;
};

#endif // UVELEMENT_H
