#ifndef UVVERTEX_H
#define UVVERTEX_H

#include <QtGlobal>

class UVVertex
{
    public:
        UVVertex(int meshID, int uvID);
        UVVertex(int meshID, int uvID, qreal x, qreal y);
        UVVertex(int meshID, int uvID, float x, float y);

        qreal GetX();
        void SetX(qreal y);
        qreal GetY();
        void SetY(qreal x);
        int* GetMeshID();
        void SetMeshID(int id);
        int* GetUvID();
        void SetUvID(int id);


    private:
        qreal x;
        qreal y;
        int meshID;
        int uvID;
};

#endif // UVVERTEX_H
