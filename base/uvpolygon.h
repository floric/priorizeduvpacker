#ifndef UVPOLYGON_H
#define UVPOLYGON_H

#include <QtGlobal>
#include <QVector>
#include <QPolygonF>
#include <QPointF>

#include "node.h"
#include "../util/uiutils.h"

class UVElement;
class UVVertex;

class UVPolygon
{
    public:
        UVPolygon();
        QVector<UVVertex*>* GetVertices();
        void AddVertex(UVVertex* vertex);
        int GetVertexCount();
        UVVertex* GetVertex(int index);
        QPolygonF GetQtPolygon();
        void SetUVElement(UVElement* element);
        UVElement* GetUVElement();
        qreal GetSurfaceArea();

    private:
        QVector<UVVertex*> vertices;
        UVElement* element;
};

#endif // UVPOLYGON_H
