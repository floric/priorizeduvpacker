#ifndef GEOMETRYMESH_H
#define GEOMETRYMESH_H

#include <QPoint>
#include <QArrayData>
#include <QString>

class GeometryMesh
{
    public:
        GeometryMesh();
        QString GetName();
        void SetName(QString name);
    private:
        QString name;
        QArrayData vertices;
};

#endif // GEOMETRYMESH_H
